/*
 * Grid State Class
 */

package com.www.bin;
/**
 *
 * @author  Bin Wang
 */
public class TDGridState {
    public int row;
    public int column;
    
    /** Creates a new instance of TDGridState */
    public TDGridState(int row, int column) {
        this.row = row;
        this.column = column;
    }
    
    public TDGridState(TDGridState other){
        this.row = other.row;
        this.column = other.column;
    }
    
    public int isOnBoundary() {
        if(column == 0) {
            if (row == TDGridSolver.GRID_ROWS-1)
                return TDGridSolver.SOUTHWEST;
            else if (row == 0)
                return TDGridSolver.NORTHWEST;
            return TDGridSolver.WEST;
        }
        else if (column == TDGridSolver.GRID_COLS-1) {
            if (row == TDGridSolver.GRID_ROWS-1)
                return TDGridSolver.SOUTHEAST;
            else if (row == 0)
                return TDGridSolver.NORTHEAST;
            return TDGridSolver.EAST;
        }
        else {
            if(row== TDGridSolver.GRID_ROWS-1)
                return TDGridSolver.SOUTH;
            if(row==0)
                return TDGridSolver.NORTH;
        }
        return -1;
    }
    
    public boolean equivalent(TDGridState other){
        if(this == other)
            return true;
        if(other.row == row &&
        other.column == column)
            return true;
        return false;
    }
    
    public boolean equals(Object obj){
        if(this == obj)
            return true;
        if((obj == null) || (obj.getClass() != this.getClass()))
            return false;
        TDGridState tmp = (TDGridState) obj;
        return equivalent(tmp);
    }
    
    public int hashCode(){
        int hash = 1;
        hash += 1 * row + 20 * column;
        return hash;
    }
    
    public String toString(){
        return "GWS{"+row+","+column+"}";
    }
    
}
