/*
 * Main class of the TD Learning Solver
 */

package com.www.bin;
import java.util.*;
/**
 *
 * @author  Bin Wang
 *
 *  The TDGridSolver
 */
public class TDGridSolver {

    //4*4 grid
    final static int GRID_ROWS = 4;
    final static int GRID_COLS = 4;
    
    //actions
    final static int NORTH = 1;
    final static int SOUTH = 2;
    final static int EAST = 3;
    final static int WEST = 4;
    //boarders
    final static int NORTHWEST = 5;
    final static int NORTHEAST = 6;
    final static int SOUTHWEST = 7;
    final static int SOUTHEAST = 8;
    //amount of actions
    final static int NUMACTIONS = 4;

    //define the rewards
    final static int REWARD_STEP = -1;//move cost
    final static int REWARD_GOAL = 100;//reward of reaching the goal
    final static int REWARD_UNSAFE_ZONE = -100;//reward of reaching the unsafe shade

    //define the start state, goal state and unsafe states
    TDGridState start = new TDGridState(3,0);
    TDGridState goal = new TDGridState(1,3);
    ArrayList<TDGridState> shades = new ArrayList<TDGridState>();

    //define the const values used for e-greedy algorithm and calculating Q value
    final static double EPSILON = 0.1;
    final static double ALPHA = 0.5;
    final static double GAMMA = 0.9;
    
    TDGridStateActionValuation valueCalculator = new TDGridStateActionValuation();
    Random uniform = new Random();
    
    /** Creates a new instance of TDGridSolver */
    public TDGridSolver(){
        //add the unsafe zones
        shades.add(new TDGridState(1, 1));
        shades.add(new TDGridState(2, 3));
        shades.add(new TDGridState(3, 1));
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Creating the TDGridSolver");
        TDGridSolver solver = new TDGridSolver();
        System.out.println("Finding the optimal path");
        solver.findOptimalPath();
        System.out.println(solver.toStringOptimalPath());
    }

    //uses the SARSA algorithm to find the optimal path
    public void findOptimalPath(){
        TDGridState state, nextState;
        int action, nextAction, reward, step;
        double v, vNext, value;
        //repeat episodes
        for(int i=0; i<100; i++){
            step = 0;
            state = new TDGridState(start);//initialize state
            action = getNextAction(state);//choose action using policy from Q
            //repeat each step of the episode
            do{
                nextState = getNextState(state, action);//get s' by taking the action
                reward = getReward(state, action);//get r by taking the action
                nextAction = getNextAction(nextState);//choose a' using policy from Q
                
                v = valueCalculator.getStateActionValue(state, action);
                vNext = valueCalculator.getStateActionValue(nextState, nextAction);

                //update Q(s, a)
                value = v + ALPHA * (reward + GAMMA * vNext - v);
                valueCalculator.addStateActionValue(new TDGridStateActionPair(state,action), value);
                state = nextState;
                action = nextAction;
                step++;
            }
            while(!state.equals(goal));
        }
    }
    //print the optimal path
    public String toStringOptimalPath(){
        //TDGridState state;
        //int action;
        StringBuffer sb = new StringBuffer();
        int grid[][] = new int[GRID_ROWS+1][GRID_COLS+1];
        TDGridState state = new TDGridState(start);
        int action, step=2;

        //put a 1 in the start position of the gridWorld array
        grid[3][0] = 1;
        //fill in the grid 2D array
        do{
            action = valueCalculator.getBestAction(state);
            state = getNextState(state, action);
            grid[state.row][state.column] = step;
            step++;
        }while(!state.equals(start) && !state.equals(goal) && step < 99);

        //print the grid 2D array
        for(int i=0; i<GRID_ROWS; i++){
            sb.append("\n"+i+"|");
            for(int j=0; j<GRID_COLS; j++){
                if(grid[i][j] == 0)
                    sb.append("  |");
                else{
                    if(grid[i][j] < 10)
                        sb.append(" "+grid[i][j]+"|");
                    else
                        sb.append(grid[i][j]+"|");
                }
            }
        }
        sb.append("\n  0  1  2  3");
        return sb.toString();
    }

    /** uses E-Greedy to select the next action based on Q */
    public int getNextAction(TDGridState current){
        double r = uniform.nextDouble();

        if(r <= EPSILON)
            return 1+uniform.nextInt(3);

        return valueCalculator.getBestAction(current);
    }

    /** get reward */
    int getReward(TDGridState current, int action) {
        if(current.equals(goal)){
            return REWARD_GOAL;
        }else if (shades.contains(current)){
            return REWARD_UNSAFE_ZONE;
        }
        return REWARD_STEP;
    }

    /** get next state */
    TDGridState getNextState(TDGridState current, int action) {
        //define what happens at boundary
        TDGridState toReturn = new TDGridState(current);
        int boundary = current.isOnBoundary();
        
        switch(action) {
            case NORTH:
                if(boundary != NORTH && boundary != NORTHWEST && boundary != NORTHEAST)
                    toReturn.row -= 1;
                break;
            case SOUTH:
                if(boundary != SOUTH && boundary != SOUTHWEST && boundary != SOUTHEAST)
                    toReturn.row += 1;
                break;
            case EAST:
                if(boundary != EAST && boundary != NORTHEAST && boundary != SOUTHEAST)
                    toReturn.column += 1;
                break;
            case WEST:
                if(boundary != WEST && boundary != NORTHWEST && boundary != SOUTHWEST)
                    toReturn.column -= 1;
                break;
        }

        //try to move to the shades
        if(shades.contains(toReturn)){
            toReturn.row = current.row;
            toReturn.column = current.column;
        }
        
        return toReturn;
    }
}
