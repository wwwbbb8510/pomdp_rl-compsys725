/*
 * Grid stat action associated pair
 */

package com.www.bin;
/**
 *
 * @author  Bin Wang
 */
public class TDGridStateActionPair {
    public TDGridState state;
    public int action;
    /** Creates a new instance of TDGridStateActionPair */
    public TDGridStateActionPair(TDGridState state, int action){
        this.state = state;
        this.action = action;
    }
    
    public boolean equals(Object obj){
        if(this == obj)
            return true;
        if((obj == null) || (obj.getClass() != this.getClass()))
            return false;
        
        TDGridStateActionPair tmp = (TDGridStateActionPair) obj;
        return (tmp.state.equivalent(state) && tmp.action == action);
    }
    
    public int hashCode(){
        int hash = state.hashCode();
        hash += action * 101;
        return hash;
    }
    
}
