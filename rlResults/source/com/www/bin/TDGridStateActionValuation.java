/*
 * Valuation Calculator
 */

package com.www.bin;
import java.util.*;
/**
 *
 * @author  Bin Wang
 */
public class TDGridStateActionValuation extends HashMap{
    TDGridStateActionPair pair = new TDGridStateActionPair(new TDGridState(0,0),0);
    /** Creates a new instance of TDGridStateActionValuation */
    public TDGridStateActionValuation() {
    }
    
    public void addStateActionValue(TDGridStateActionPair pair, Double value){
        put(pair, value);
    }
    
    public double getStateActionValue(TDGridState state, int action){
        pair.state = state;
        pair.action = action;
        
        if(containsKey(pair))
            return ((Double)get(pair)).doubleValue();
        return 0.0;
    }
    
    public int getBestAction(TDGridState state){
        double values[] = new double[TDGridSolver.NUMACTIONS+1];
        Double value;
        
        TDGridStateActionPair pair = new TDGridStateActionPair(state,0);
        for (int i=1; i<= TDGridSolver.NUMACTIONS; i++){
            pair.action = i;
            value = (Double)get(pair);
            if(value != null)
                values[i] = value.doubleValue();
        }
        return argmaxStartOne(values);
    }

   int argmaxStartOne(double values[]){
        int maxIndex=1;
        double maxValue=values[1];
        for(int i=2; i<values.length; i++){
            if(values[i] > maxValue){
                maxValue = values[i];
            }
        }
        ArrayList<Integer> arrMaxIndexes = new ArrayList<Integer>();
        for (int j=1; j<values.length; j++){
            if(values[j] == maxValue){
                arrMaxIndexes.add(j);
            }
        }
        maxIndex = arrMaxIndexes.get(new Random().nextInt(arrMaxIndexes.size()));
        return maxIndex;
    }
}
